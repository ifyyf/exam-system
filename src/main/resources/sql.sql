DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int unsigned NOT NULL,
  `account` varchar(31) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '登陆用户名',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表';

INSERT INTO `sys_user` VALUES (1, 'if123', '5ffbb3323e89ceb03df4076de3e1a719');

DROP TABLE IF EXISTS `sys_question`;
CREATE TABLE `sys_question`  (
  `id` int unsigned NOT NULL COMMENT 'id',
  `title` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '题目',
  `answer` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '答案',
  `picture` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '题目表';

INSERT INTO `sys_question` VALUES (1, '右图是以下哪个语言的Logo?<br>   A.C<br>   B.Java<br>   C.PHP<br>   D.VB', 'B', 'java.jpg');
INSERT INTO `sys_question` VALUES (2, '右图是以下哪个数据库的Logo?<br>   A.MySQL<br>   B.Oracle<br>   C.SqlServer<br>   D.DB2', 'A', 'mysql.jpg');
INSERT INTO `sys_question` VALUES (3, '\r\n右图是以下哪个服务器的Logo?<br>   A.JBoss<br>   B.Oracle<br>   C.Jetty<br>   D.Tomcat', 'D', 'tomcat.jpg');
INSERT INTO `sys_question` VALUES (4, '\r\n以下哪个是Java的基本数据类型?<br>   A.String<br>   B.Integer<br>   C.boolean<br>   D.Math', 'C', '');
INSERT INTO `sys_question` VALUES (5, '\r\n以下哪个是Java的引用数据类型?<br>   A.String<br>   B.int<br>   C.boolean<br>   D.char', 'A', '');
INSERT INTO `sys_question` VALUES (6, '\r\n以下哪不是Object类中的方法?<br>   A.equals<br>   B.hashCode<br>   C.toString<br>   D.compareTo', 'D', '');
INSERT INTO `sys_question` VALUES (7, '\r\n以下哪个不是String类中的方法?<br>   A.charAt<br>   B.reverse<br>   C.replace<br>   D.indexOf', 'B', '');
INSERT INTO `sys_question` VALUES (8, '\r\n以下哪个是Java中非法标识符?<br>   A.variable<br>   B.variable2<br>   C.%variable<br>   D._variable', 'C', '');
INSERT INTO `sys_question` VALUES (9, '\r\n以下哪个选项是用于编译Java文件的?<br>   A.javac<br>   B.java<br>   C.path<br>   D.classPath', 'A', '');
INSERT INTO `sys_question` VALUES (10, '\r\n经过编译后形成的文件后缀名?<br>   A. .obj<br>   B. .exe<br>   C. .class<br>   D. .java', 'C', '');
INSERT INTO `sys_question` VALUES (11, '\r\n未编译之前的源文件后缀名?<br>   A. .obj<br>   B. .exe<br>   C. .class<br>   D. .java', 'D', '');
INSERT INTO `sys_question` VALUES (13, '\r\n下面哪个选项是key-value形式?<br>   A.ArrayList<br>   B.HashMap<br>   C.LinkedList<br>   D.Vector', 'B', '');