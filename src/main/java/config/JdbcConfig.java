package config;

import lombok.Data;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Properties;
import java.util.Set;

/**
 * @author if
 * Description: 读取jdbc.properties中的配置
 * 可优化的地方：多次开启关闭io
 * date: 2021/6/18 上午 08:54
 */
@Data
public class JdbcConfig {
    private static String username = getPropertiesValue("jdbc.username");
    private static String password = getPropertiesValue("jdbc.password");
    private static String driverClassName = getPropertiesValue("jdbc.driverClassName");
    private static String url = getPropertiesValue("jdbc.url");
    private static final String PROPERTIES_NAME = "jdbc.properties";
    private static String dataBase;
    private static Connection connection=null;
    private static Statement stmt=null;
    /**
     * 获取资源文件内容
     * @param urlName
     * @return
     */
    public static String getPropertiesValue(String urlName) {
        String url = null;
        InputStream in=null;
        try {
            ClassLoader classLoader = JdbcConfig.class.getClassLoader();
            Properties prop = new Properties();
            //读取属性文件jdbc.properties
            in = classLoader.getResourceAsStream(PROPERTIES_NAME);
            //加载属性列表
            prop.load(in);
            Set<String> propertyNames = prop.stringPropertyNames();
            if(propertyNames.contains(urlName)){
                url = prop.getProperty(urlName);
            }
            return url;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            try {
                in.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取JDBC连接
     * @return
     */
    public static Connection getConnection(){
        try {
            if(connection==null){
                //加载驱动程序：它通过反射创建一个driver对象。
                Class.forName(driverClassName);
                //获得数据连接对象。
                // 在返回connection对象之前，DriverManager它内部会先校验驱动对象driver信息对不对,我们只要知道内部过程即可。
                connection= DriverManager.getConnection(url,username,password);
            }
           return connection;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取stmt对象操作数据库
     * @return
     */
    public static Statement getStatement(){
        try{
            if(stmt==null){
                stmt=getConnection().createStatement();
            }
            return stmt;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static PreparedStatement getPreparedStatement(String sql){
        try{
            return getConnection().prepareStatement(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String getDataBase() {
        if(dataBase==null){
            String[] strings=url.split("\\?")[0].split("/");
            dataBase=strings[strings.length-1];
        }
        return dataBase;
    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public static String getDriverClassName() {
        return driverClassName;
    }

    public static String getUrl() {
        return url;
    }

    public static String getPropertiesName() {
        return PROPERTIES_NAME;
    }
}
