import domain.User;
import mapper.JdbcMapper;
import mapper.QueryMapper;
import view.LoginFrame;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 请在此开始运行
 */
public class MainLogin {
    public static void main(String [] args){
        new LoginFrame("考试系统");
//        test();
    }
    //测试JdbcUtils
    private static void test(){
        QueryMapper<User> queryMapper=new QueryMapper<>(User.class);
        QueryMapper<User> queryMapper2=new QueryMapper<>(User.class);
        QueryMapper<User> queryMapper3=new QueryMapper<>(User.class);
        QueryMapper<User> queryMapper4=new QueryMapper<>(User.class);
        JdbcMapper<User> jdbcMapper=new JdbcMapper<>(User.class,"sys_user");
        JdbcMapper<User> jdbcMapper2=new JdbcMapper<>(User.class,"sys_user");
        JdbcMapper<User> jdbcMapper3=new JdbcMapper<>(User.class,"sys_user");
        JdbcMapper<User> jdbcMapper4=new JdbcMapper<>(User.class,"sys_user");
        try{
            //多个参数形式
            queryMapper.eq("account","if123")
                    .eq("password","if123");
            User user = jdbcMapper.selectOne(queryMapper);
            System.out.println("user = "+user);
            jdbcMapper.clear();
            queryMapper.clear();
            //单个参数形式
            queryMapper3.eq("account","if123");
            User user1 = jdbcMapper2.selectOneByOneFiled(queryMapper3);
            System.out.println("user1 = "+user1);
            jdbcMapper2.clear();
            queryMapper3.clear();
            //以map形式
            Map<String,Object> map=new HashMap<>();
            map.put("account","if123");
            queryMapper2.allEq(map);
            User user2 = jdbcMapper3.selectOne(queryMapper2);
            System.out.println("user2 = "+user2);
            queryMapper2.clear();
            jdbcMapper3.clear();
            //查询list
            queryMapper4.eq("account","if123");
            List<User> users = jdbcMapper4.selectList(queryMapper4);
            System.out.println("users = "+users);
            jdbcMapper4.clear();
            queryMapper4.clear();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
