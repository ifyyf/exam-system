package domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {
    private String title;
    private String answer;
    private String picture;

    public Question(String title, String answer) {
        this.title = title;
        this.answer = answer;
    }
}
