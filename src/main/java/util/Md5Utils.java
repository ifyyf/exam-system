package util;

import org.apache.shiro.crypto.hash.Md5Hash;

import java.util.UUID;

/**
 * @author if
 * Description: What is it?
 * date: 2021/6/18 上午 08:45
 */
public class Md5Utils {
    static final int HASH_ITERATIONS = 1024;
    /**
     * 生成盐
     */
    public static String createSalt(){
        return UUID.randomUUID().toString().replace("-","").toUpperCase();
    }

    /**
     * 生成加密字符串
     */
    public static String md5(String source,String salt){
        return new Md5Hash(source,salt,HASH_ITERATIONS).toString();
    }

    /**
     * 生成6位验证码
     * @return
     */
    public static String createCode(){
        return createSalt().substring(0,6);
    }
}
