package util;

import domain.Question;
import mapper.JdbcMapper;
import mapper.QueryMapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;

//缓存题目
public class QuestionFileReader {
    private final String surface="sys_question";
    private int current=0;
    private final int count =50;
    //无重复的读取题目
    private HashSet<Question> questionBox = new HashSet();
    {
        QueryMapper<Question> queryMapper=new QueryMapper<>(Question.class);
        JdbcMapper<Question> jdbcMapper=new JdbcMapper<>(Question.class,surface);
        List<Question> questions = jdbcMapper.selectAll(queryMapper, current, count);
        questionBox.addAll(questions);
    }

    //获取缓存
    public HashSet<Question> getQuestionBox(){
        return questionBox;
    }
}
