package dao;

import domain.User;
import mapper.JdbcMapper;
import mapper.QueryMapper;
import util.UserFileReader;

//持久层
//数据的持久化
//只负责数据的读 写  不负责处理逻辑
public class UserDao {
    private  final String surface="sys_user";

    public User getUserByUsername(String account){
        QueryMapper<User> queryMapper=new QueryMapper<>(User.class);
        JdbcMapper<User> jdbcMapper=new JdbcMapper<>(User.class,surface);
        queryMapper.eq("account",account);
        return jdbcMapper.selectOne(queryMapper);
    }
}
