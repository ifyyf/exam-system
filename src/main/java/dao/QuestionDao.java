package dao;

import domain.Question;
import util.MySpring;
import util.QuestionFileReader;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

//用来获取题目
public class QuestionDao {
    //获取缓存对象
    private QuestionFileReader questionFileReader = MySpring.getBean("util.QuestionFileReader");
    //将缓存的对象 存入 可以通过索引随机生成题目
    private ArrayList<Question> questionBank = new ArrayList<>(questionFileReader.getQuestionBox());

    public ArrayList<Question> getPaper(){
        return questionBank;
    }

}
