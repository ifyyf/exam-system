package service;
//业务层 负责处理读到的数据

import dao.UserDao;
import domain.User;
import util.Md5Utils;
import util.MySpring;

//控制登录
public class UserService {
    private String salt="salt213&sd";
    private UserDao userDao =  MySpring.getBean("dao.UserDao");

    public String login(String account,String password){
        User user=userDao.getUserByUsername(account);
        if(user.getPassword().equals(Md5Utils.md5(password,salt))){
            return "登录成功";
        }
        return "账号或用户名错误";
    }
}
